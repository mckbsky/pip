package builder;

public class Student extends Osoba {

    private Integer nrIndeksu;

    static StudentBuilder builder() {
        return new StudentBuilder();
    }

    public Student(String imie, String nazwisko, Integer nrIndekstu) {
        super(imie, nazwisko);
        this.nrIndeksu = nrIndekstu;
    }


    @Override
    public String toString() {
        return super.toString() + ", nrIndeksu[" + nrIndeksu + "]";
    }
}
