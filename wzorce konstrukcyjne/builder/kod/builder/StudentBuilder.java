package builder;

public class StudentBuilder {

    private String imie;
    private String nazwisko;
    private Integer nrIndeksu;

    StudentBuilder nrIndeksu(Integer nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
        return this;
    }

    public StudentBuilder imie(String imie) {
        this.imie = imie;
        return this;
    }

    private StudentBuilder nazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
        return this;
    }

    private Student build() {
        return new Student(imie, nazwisko, nrIndeksu);
    }

    public static void main(String[] args) {
        Student student = Student.builder()
                .imie("Maciej")
                .nazwisko("Brzeczkowski")
                .nrIndeksu(803501)
                .build();

        System.out.println(student.toString());
    }

}
