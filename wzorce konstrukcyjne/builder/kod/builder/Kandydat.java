package builder;

public class Kandydat extends Osoba {

    private Integer iloscPunktow;

    Kandydat(String imie, String nazwisko, Integer iloscPunktow) {
        super(imie, nazwisko);
        this.iloscPunktow = iloscPunktow;
    }

    static KandydatBuilder builder() {
        return new KandydatBuilder();
    }

    @Override
    public String toString() {
        return super.toString() + ", iloscPunktow[" + iloscPunktow + "]";
    }
}
