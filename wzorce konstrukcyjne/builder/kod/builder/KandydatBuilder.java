package builder;

public class KandydatBuilder {

    private String imie;
    private String nazwisko;
    private Integer iloscPunktow;


    KandydatBuilder iloscPunktow(Integer iloscPunktow) {
        this.iloscPunktow = iloscPunktow;
        return this;
    }

    public KandydatBuilder imie(String imie) {
        this.imie = imie;
        return this;
    }

    private KandydatBuilder nazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
        return this;
    }

    private Kandydat build() {
        return new Kandydat(imie, nazwisko, iloscPunktow);
    }

    public static void main(String[] args) {

        Kandydat kandydat = Kandydat.builder()
                .imie("Adam")
                .nazwisko("Bebenek")
                .iloscPunktow(90)
                .build();

        System.out.println(kandydat.toString());

    }
}
