package builder;

abstract class Osoba {

    protected String imie;
    private String nazwisko;

    Osoba(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        return "Imie[" + imie + "], nazwisko[" + nazwisko + "]";
    }
}
