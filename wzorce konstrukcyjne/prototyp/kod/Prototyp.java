public interface Prototyp<T> extends Cloneable {

    T clone() throws CloneNotSupportedException;

}