import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class Dziekanat {
    private static Integer nrIndeksu = 1;

    private List<Student> przeprowadzRekrutacje(List<Kandydat> kandydaci) throws CloneNotSupportedException {
        Student studentPrototyp = new Student();

        kandydaci = kandydaci.stream()
                .filter(kandydat -> kandydat.getIloscPunktow() >= 50)
                .collect(Collectors.toList());

        ArrayList<Student> studenci = new ArrayList<>();
        for(Kandydat kandydat: kandydaci) {
            Student nowyStudent = studentPrototyp.clone();
            nowyStudent.setImie(kandydat.getImie());
            nowyStudent.setNrIndeksu(nrIndeksu++);
            studenci.add(nowyStudent);
        }

        return studenci;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Dziekanat dziekanat = new Dziekanat();
        Kandydat kandydatPrototyp = new Kandydat();

        Kandydat kandydat1 = kandydatPrototyp.clone();
        kandydat1.set("Maciek", 30);
        Kandydat kandydat2 = kandydatPrototyp.clone();
        kandydat2.set("Adam", 40);
        Kandydat kandydat3 = kandydatPrototyp.clone();
        kandydat3.set("Mateusz", 50);
        Kandydat kandydat4 = kandydatPrototyp.clone();
        kandydat4.set("Mariusz", 100);

        List<Kandydat> kandydaci = Arrays.asList(kandydat1, kandydat2, kandydat3, kandydat4);

        System.out.println("Przyjeci studenci: ");
        dziekanat.przeprowadzRekrutacje(kandydaci).forEach(System.out::println);
    }
}
