import lombok.Data;

@Data
public class Student implements Prototyp<Student> {
    private String imie;
    private Integer nrIndeksu;

    @Override
    public Student clone() throws CloneNotSupportedException {
        return (Student) super.clone();
    }

    @Override
    public String toString() {
        return "Imie: " + imie + ", nrIndeksu: " + nrIndeksu;
    }

}