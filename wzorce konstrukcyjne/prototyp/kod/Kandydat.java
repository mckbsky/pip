import lombok.Data;

@Data
public class Kandydat implements Prototyp<Kandydat> {
    private String imie;
    private Integer iloscPunktow;

    public void set(String imie, Integer iloscPunktow) {
        this.imie = imie;
        this.iloscPunktow = iloscPunktow;
    }

    @Override
    public Kandydat clone() throws CloneNotSupportedException {
        return (Kandydat) super.clone();
    }
}
