package factorymethod;

abstract class Wniosek {


}


class WniosekStypendiumRektora extends Wniosek{
    public WniosekStypendiumRektora(){
        System.out.println("Stypendium Rektora");
    }

}

class WniosekStypendiumSocjalne extends Wniosek{
    public WniosekStypendiumSocjalne(){
        System.out.println("Stypendium Socjalne");
    }

}

class WniosekPowtarzaniePrzedmiotu extends Wniosek{
    public WniosekPowtarzaniePrzedmiotu(){
        System.out.println("Powtarzanie przedmiotu");
    }

}

class WniosekFactory{

    public enum WniosekType {
        STYPENDIUM_REKTORA,
        STYPENDIUM_SOCJALNE,
        POWTARZANIE_PRZEDMIOTU
    }

    public Wniosek stworzWniosek(WniosekType wniosekType){
        switch (wniosekType) {
            case STYPENDIUM_REKTORA:
                return new WniosekStypendiumRektora();
            case STYPENDIUM_SOCJALNE:
                return new WniosekStypendiumSocjalne();
            case POWTARZANIE_PRZEDMIOTU:
                return new WniosekPowtarzaniePrzedmiotu();
        }
        return null;
    }

}


class Student {

    public static void main(String[]args){

        WniosekFactory wniosekFactory= new WniosekFactory();

        Wniosek wniosek1 = wniosekFactory.stworzWniosek(WniosekFactory.WniosekType.STYPENDIUM_SOCJALNE);

        Wniosek wniosek2 = wniosekFactory.stworzWniosek(WniosekFactory.WniosekType.STYPENDIUM_SOCJALNE);

        Wniosek wniosek3 = wniosekFactory.stworzWniosek(WniosekFactory.WniosekType.POWTARZANIE_PRZEDMIOTU);
    }
}