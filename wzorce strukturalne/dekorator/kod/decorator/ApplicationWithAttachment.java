package decorator;

class ApplicationWithAttachment extends ApplicationDecorator {

    ApplicationWithAttachment(Submission submission) {
        super(submission);
    }

    @Override
    public void submit() {
        super.submit();
        System.out.println("With attachment");
    }
}
