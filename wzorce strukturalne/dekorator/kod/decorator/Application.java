package decorator;

public class Application implements Submission {

    @Override
    public void submit() {
        System.out.println("Application submitted");
    }
}
