package decorator;

public interface Submission {

    void submit();

}
