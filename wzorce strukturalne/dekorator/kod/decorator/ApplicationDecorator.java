package decorator;

abstract class ApplicationDecorator implements Submission {

    private Submission submission;

    ApplicationDecorator(Submission submission) {
        this.submission = submission;
    }

    @Override
    public void submit() {
        submission.submit();
    }
}
