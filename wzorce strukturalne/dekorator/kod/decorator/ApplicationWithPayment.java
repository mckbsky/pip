package decorator;

public class ApplicationWithPayment extends ApplicationDecorator {

    ApplicationWithPayment(Submission submission) {
        super(submission);
    }

    @Override
    public void submit() {
        super.submit();
        System.out.println("With payment");
    }

}
