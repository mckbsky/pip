package decorator;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private List<Submission> submissions = new ArrayList<>();

    private void submitApplications() {
        submissions.forEach(Submission::submit);
    }

    private void addApplication(Submission submission) {
        this.submissions.add(submission);
    }

    public static void main(String[] args) {

        Student student = new Student();
        student.addApplication(new ApplicationWithAttachment(new Application()));
        student.addApplication(new ApplicationWithAttachment(new ApplicationWithPayment(new Application())));
        student.submitApplications();

    }
}
