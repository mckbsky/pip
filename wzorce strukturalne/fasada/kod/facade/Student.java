package facade;

class Student {
    private int studentId;
    private String name;
    private String surname;
    public Student(int id) {
        this.studentId = id;
    }

    public int getStudentId() {
        return this.studentId;
    }

    public static void main(String[] args) {
        Student student = new Student(123);
        Facade facade = new Facade();
        facade.oplacCzesne(student.getStudentId(), 120.30);
    }
};

