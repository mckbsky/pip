package facade;

import facade.ksiegowosc.Ksiegowa;
import facade.ksiegowosc.Wplata;

public class Facade {
    public Wplata oplacCzesne(int idStudent, double kwota) {
        Ksiegowa ksiegowa = new Ksiegowa();
        ksiegowa.sprawdzDane();
        Wplata wplata = ksiegowa.zaksiegujWplate(kwota);
        ksiegowa.potwierdzWplate();
        return wplata;
    }

}



