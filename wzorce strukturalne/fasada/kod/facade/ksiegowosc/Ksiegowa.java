package facade.ksiegowosc;

public class Ksiegowa {

    public void sprawdzDane() {
        System.out.println("Sprawdzam dane...");
    }

    public Wplata zaksiegujWplate(double kwota) {
        System.out.println("Ksieguje wplate...");
        Wplata wplata = new Wplata();
        wplata.setKwota(kwota);
        return wplata;
    }

    public void potwierdzWplate() {
        System.out.println("Potwierdzenie wplaty.");
    }
}

