package facade.ksiegowosc;

public class Wplata {
    private double kwota;

    public void setKwota(double k) {
        this.kwota = k;
    }

    public Double getKwota() {
        return this.kwota;
    }
}

