package facade.dziekanat;

public class PracownikDziekanatu {

    public void sprawdzDokumenty() {
        System.out.println("Sprawdzam dane");
    }

    public Wniosek przyjmijWniosek() {
        System.out.println("Przyjmuje wniosek");
        Wniosek wniosek = new Wniosek();
        
        return wniosek;
    }

}

