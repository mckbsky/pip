package strategy;

public class OralExam implements ExamStrategy {
    @Override
    public void carryOut() {
        System.out.println("Oral exam");
    }
}
