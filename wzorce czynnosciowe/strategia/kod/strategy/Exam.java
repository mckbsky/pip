package strategy;

public class Exam {

    private ExamStrategy examStrategy;
    private Integer term;

    void setExamStrategy(ExamStrategy examStrategy) {
        this.examStrategy = examStrategy;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public void carryOut() {
        System.out.print("Term " + term + ": ");
        examStrategy.carryOut();
    }
}
