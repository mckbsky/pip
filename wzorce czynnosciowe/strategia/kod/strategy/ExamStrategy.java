package strategy;

public interface ExamStrategy {

    void carryOut();

}
