package strategy;

public class WrittenExam implements ExamStrategy {

    @Override
    public void carryOut() {
        System.out.println("Written exam");
    }

}
