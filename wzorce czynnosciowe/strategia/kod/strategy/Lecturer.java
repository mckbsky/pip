package strategy;

public class Lecturer {

    private void carryOutExam() {
        Exam exam = new Exam();
        exam.setTerm(1);
        exam.setExamStrategy(new WrittenExam());
        exam.carryOut();

        exam.setTerm(2);
        exam.setExamStrategy(new OralExam());
        exam.carryOut();
    }

    public static void main(String[] args) {
        Lecturer lecturer = new Lecturer();
        lecturer.carryOutExam();
    }

}
