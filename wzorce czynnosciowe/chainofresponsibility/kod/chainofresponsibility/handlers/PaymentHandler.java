package chainofresponsibility.handlers;

import chainofresponsibility.DeaneryRequestHandler;
import chainofresponsibility.requests.PaymentRequest;
import chainofresponsibility.requests.Request;

public class PaymentHandler extends DeaneryRequestHandler {

    @Override
    protected Boolean handleRequest(Request request) {
        if (!(request instanceof PaymentRequest)) {
            return Boolean.FALSE;
        }

        System.out.println("[PaymentHandler] - request handled");
        return Boolean.TRUE;
    }
}
