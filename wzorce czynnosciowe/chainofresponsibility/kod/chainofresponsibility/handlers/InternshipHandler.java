package chainofresponsibility.handlers;

import chainofresponsibility.DeaneryRequestHandler;
import chainofresponsibility.requests.InternshipRequest;
import chainofresponsibility.requests.Request;

public class InternshipHandler extends DeaneryRequestHandler {

    @Override
    protected Boolean handleRequest(Request request) {
        if (!(request instanceof InternshipRequest)) {
            return Boolean.FALSE;
        }

        System.out.println("[InternshipHandler] - request handled");
        return Boolean.TRUE;
    }

}
