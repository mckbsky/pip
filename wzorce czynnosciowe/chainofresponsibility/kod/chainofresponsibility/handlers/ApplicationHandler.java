package chainofresponsibility.handlers;


import chainofresponsibility.DeaneryRequestHandler;
import chainofresponsibility.requests.ApplicationRequest;
import chainofresponsibility.requests.Request;

public class ApplicationHandler extends DeaneryRequestHandler {

    @Override
    protected Boolean handleRequest(Request request) {
        if (!(request instanceof ApplicationRequest)) {
            return Boolean.FALSE;
        }

        System.out.println("[ApplicationHandler] - request handled");
        return Boolean.TRUE;
    }

}
