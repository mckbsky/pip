package chainofresponsibility;

import chainofresponsibility.requests.Request;

public class DeaneryRequestHandler {

    private DeaneryRequestHandler nextHandler;

    public DeaneryRequestHandler addHandler(DeaneryRequestHandler deaneryEmployee) {
        if(nextHandler == null)
            nextHandler = deaneryEmployee;
        else
            nextHandler.addHandler(deaneryEmployee);
        return this;
    }

    public void handle(Request request) {
        if(!handleRequest(request)) {
            if(nextHandler == null)
                throw new IllegalArgumentException("Request cant be handled");

            nextHandler.handle(request);
        }
    }

    protected Boolean handleRequest(Request request) {
        return nextHandler.handleRequest(request);
    }

}
