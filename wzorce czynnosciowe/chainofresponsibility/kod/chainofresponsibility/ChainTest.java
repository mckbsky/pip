package chainofresponsibility;

import chainofresponsibility.handlers.ApplicationHandler;
import chainofresponsibility.handlers.InternshipHandler;
import chainofresponsibility.handlers.PaymentHandler;
import chainofresponsibility.requests.ApplicationRequest;
import chainofresponsibility.requests.InternshipRequest;
import chainofresponsibility.requests.PaymentRequest;

import java.util.Arrays;

public class ChainTest {

    public static void main(String[] args) {
        DeaneryRequestHandler deaneryRequestHandler = new DeaneryRequestHandler()
                .addHandler(new PaymentHandler())
                .addHandler(new ApplicationHandler())
                .addHandler(new InternshipHandler());

        Arrays.asList(
                new InternshipRequest(),
                new ApplicationRequest(),
                new PaymentRequest()
        ).forEach(deaneryRequestHandler::handle);
    }
}
