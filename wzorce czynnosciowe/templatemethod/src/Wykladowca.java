import java.time.LocalDateTime;

public class Wykladowca {

    public void przeprowadzEgzamin(){
        Egzamin egzaminPisemny = new EgzaminPisemny();
        egzaminPisemny.setTermin(1);
        egzaminPisemny.ustalDate(LocalDateTime.now());
        egzaminPisemny.przeprowadz();

        Egzamin egzaminUstny = new EgzaminUstny();
        egzaminUstny.setTermin(2);
        egzaminUstny.ustalDate(LocalDateTime.now());
        egzaminUstny.przeprowadz();
    }

    public static void main(String []args) {
        Wykladowca wykladowca = new Wykladowca();
        wykladowca.przeprowadzEgzamin();
    }
}
