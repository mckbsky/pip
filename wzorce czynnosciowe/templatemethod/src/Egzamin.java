import java.time.LocalDateTime;

abstract class Egzamin {

    private int termin;

    private LocalDateTime data;

    abstract  protected  void przeprowadz();

    public void ustalDate(LocalDateTime data){
        this.data = data;
    }

    public void setTermin(int termin){
        this.termin = termin;
    }
}
